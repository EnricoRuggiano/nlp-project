import numpy as np
import modules.importer as imp

char2idx_dict = dict()
idx2char_dict = dict()

c_target2idx_dict = dict()
idx2c_target_dict = dict()

max_length = 0

def read_data(path): 
    chars = []
    labels = []
    sentences = []

    s_buffer = []

    data_file = open(path, 'r')

    for line in data_file:
        line = line.rstrip('\n\r')
        line = line.split()

        if line == []:
            sentences.append(s_buffer)
            s_buffer = []
        else:
            l_chars = list(line[0])
            l_label = line[-1]

            labels.append(l_label)
            
            for char in l_chars:
                chars.append(char)
                s_buffer.append([char, l_label])
    return chars, labels, sentences

def train_data(path): 
    chars, labels, sentences = read_data(path)
    imp.one_hot_encoding(chars, char2idx_dict, idx2char_dict, True)
    imp.one_hot_encoding(labels, c_target2idx_dict, idx2c_target_dict, False)

    global max_length
    #max_length = 2 * max([len(s) for s in sentences])
    max_length = 857
    x_train, y_train = imp.encode_sentences(sentences, max_length, char2idx_dict, c_target2idx_dict)

    y_train = np.expand_dims(y_train, 2)
    return x_train, y_train

def test_data(path):
    charss, labels, sentences = read_data(path)

    x_test, y_test = imp.encode_sentences(sentences, max_length, char2idx_dict, c_target2idx_dict)

    y_test = np.expand_dims(y_test, 2)
    return x_test, y_test
