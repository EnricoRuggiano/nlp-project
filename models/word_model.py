#!/usr/bin/python3

# go to parent directory
import modules.evaluation as evl 
import modules.importer as imp

import numpy as np

# config tf for GPU
import tensorflow as tf
configT = tf.ConfigProto()
configT.gpu_options.allow_growth = True
session = tf.Session(config=configT)

# path for saved weigths
import os.path
path = "saved_models/word_model.h5"
condition = os.path.exists(path)

# load data 
importer = imp.importer()
importer.run()

x_train, y_train = (importer.x_train, importer.y_train)
x_test, y_test = (importer.x_test, importer.y_test)

(batch_size, input_size) = x_train.shape

# print data info
importer.info()

from keras.models import Sequential, Model
from keras.layers import Input, TimeDistributed, Embedding, Dense, Bidirectional, LSTM, CuDNNLSTM, Dropout

units = 10
max_voc = len(importer.word2idx_dict)
label_size = len(importer.target2idx_dict)


#word model
def word_model():

    (batch_size, input_size) = x_train.shape
    word_model_1 = Input(shape = (input_size, ), dtype='int32')
    word_model_2 = Embedding(max_voc, 10, mask_zero=False)(word_model_1)
    word_model_3 = Bidirectional(LSTM(10, return_sequences = True))(word_model_2)
    word_model_4 = Dropout(0.5)(word_model_3)
    word_model_5 = TimeDistributed(Dense(label_size, activation='softmax'))(word_model_4)
    word_model = Model(word_model_1, word_model_5)
    return word_model

def less_data(s_train_limit=1000, w_limit=0.25,c_limit=0.20):
    global x_train, y_train, x_test, y_test
    train_num_sent = x_train.shape[0]
    test_num_sent = x_test.shape[0]
    print("num of sentences:\t train {}; test {}".format(train_num_sent, test_num_sent))

	# limit sentences
    factor = train_num_sent / test_num_sent
    print("num train sentences on num test sentences:\t {}".format(factor))
    s_test_limit = int(s_train_limit / factor)
    print("limited dataset to: train - from {} to {}; test - from {} to {}"\
    .format(x_train.shape[0], s_train_limit, x_test.shape[0], s_test_limit))
    
    # limit words
    word_limit = int(x_train.shape[1] * w_limit)
    print("limited words in colon from {} to {}".format(x_train.shape[1], word_limit))
    
    # limit chars
    if(len(x_train.shape) > 2):
        char_limit = int(x_train.shape[2] * c_limit)
        print("limited chars in colon from {} to {}".format(x_train.shape[2], char_limit))
        
        # limit all
        x_train = x_train[:s_train_limit, -word_limit:, -char_limit:]
        y_train = y_train[:s_train_limit, -word_limit:, -char_limit:]

        x_test = x_test[:s_test_limit, -word_limit:, -char_limit:]
        y_test = y_test[:s_test_limit, -word_limit:, -char_limit:]
    
    else:
        # limit all
        x_train = x_train[:s_train_limit, -word_limit:]
        y_train = y_train[:s_train_limit, -word_limit:]
        
        x_test = x_test[:s_test_limit, -word_limit:]
        y_test = y_test[:s_test_limit, -word_limit:]

    return

def print_prediction_word(sent, prediction):
    print(prediction.shape)
    for idx in range(prediction.shape[0]):
        min_label = np.argmin(prediction[idx])
        max_label = np.argmax(prediction[idx])
        
        #readable
        #h_word = [importer.idx2char_dict[char] for char in sent[idx]]
        h_word = importer.idx2word_dict[sent[idx]]
        h_min_label = importer.idx2target_dict[min_label]
        h_max_label = importer.idx2target_dict[max_label]
        
        #print 
        print("word\n\t{}\n\t{}\nmin label:\n\t{}\n\t{}\nmax label\n\t{}\n\t{}\n".format(sent[idx], h_word,\
            min_label, h_min_label, max_label, h_max_label))

def try_prediction(model):
    my_inp = np.array(x_train[10:11])     
    prediction = model.predict(my_inp)
    
    print_prediction_word(my_inp[0], prediction[0])
    
#run it
def try_model(limit=False):
    # workaround for the third dimension
    #import numpy as np
   
    if limit:
        less_data()
        print("new dataset shapes. train: x {}, y {}".format(x_train.shape, y_train.shape))
        print("new dataset shapes. test: x {}, y {}".format(x_test.shape, y_test.shape))
       
    # model 
    model = word_model()
    
    # load weigths
    if os.path.exists(path):
    
        from keras.models import load_model
        print("Resume",)
        model.load_weights(path)	
        print(" -> Done")
    
    model.summary()
    model.compile('adam', 'sparse_categorical_crossentropy', metrics=['accuracy'])
    
    # evaluation
    ref = y_test[x_test > 0]
    #refV = validY[x_train[0] > 0]
    refT = y_train[x_train > 0]

    evalFinal = evl.evalModel(ref,refT,refT, x_test,x_train,x_train,path, max=True)
    finalhyp = evalFinal.getBestHyp()
    #print ('Final result {:4.2f} '.format(f1_score(ref,finalhyp,average='micro')*100))

    # train
    model.fit(x_train, y_train, epochs=1, batch_size=1, callbacks=[evalFinal])#validation_split=0.1
    try_prediction(model)
		
if __name__ == "__main__":
    try_model(True)
