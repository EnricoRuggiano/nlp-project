#!/bin/sh
today=`date +"%Y-%m-%d"`
mkdir logs/$today

# media
python3 baseline_media.py > logs/$today/baseline_media.log
for file in solutions/$today/baseline_media_train*; do
        echo $file >> solutions/$today/score_baseline_media_train.txt
	./colleval.sh $file >> solutions/$today/score_baseline_media_train.txt
        echo "\n" >> solutions/$today/score_baseline_media_train.txt
done

for file in solutions/$today/baseline_media_test*; do
        echo $file >> solutions/$today/score_baseline_media_test.txt
	./colleval.sh $file >> solutions/$today/score_baseline_media_test.txt
        echo "\n" >> solutions/$today/score_baseline_media_test.txt
done


python3 kim_crf_media.py > logs/$today/kim_crf_media.log

for file in solutions/$today/kim_crf_media_train*; do
        echo $file >> solutions/$today/score_kim_crf_media_train.txt
	./colleval.sh $file >> solutions/$today/score_kim_crf_media_train.txt
        echo "\n" >> solutions/$today/score_kim_crf_media_train.txt
done

for file in solutions/$today/kim_crf_media_test*; do
        echo $file >> solutions/$today/score_kim_crf_media_test.txt
	./colleval.sh $file >> solutions/$today/score_kim_crf_media_test.txt
        echo "\n" >> solutions/$today/score_kim_crf_media_test.txt
done

python3 emb_crf_media.py > logs/$today/emb_crf_media.log
for file in solutions/$today/emb_crf_media_train*; do
        echo $file >> solutions/$today/score_emb_crf_media_train.txt
	./colleval.sh $file >> solutions/$today/score_emb_crf_media_train.txt
        echo "\n" >> solutions/$today/score_emb_crf_media_train.txt
done

for file in solutions/$today/emb_crf_media_test*; do
        echo $file >> solutions/$today/score_emb_crf_media_test.txt
	./colleval.sh $file >> solutions/$today/score_emb_crf_media_test.txt
        echo "\n" >> solutions/$today/score_emb_crf_media_test.txt
done

#one hot encoding
python3 one_hot_crf_media.py > logs/$today/one_hot_crf_media.log
for file in solutions/$today/one_hot_crf_media_train*; do
        echo $file >> solutions/$today/score_one_hot_crf_media_train.txt
	./colleval.sh $file >> solutions/$today/score_one_hot_crf_media_train.txt
        echo "\n" >> solutions/$today/score_one_hot_crf_media_train.txt
done
for file in solutions/$today/one_hot_crf_media_test*; do
        echo $file >> solutions/$today/score_one_hot_crf_media_test.txt
	./colleval.sh $file >> solutions/$today/score_one_hot_crf_media_test.txt
        echo "\n" >> solutions/$today/score_one_hot_crf_media_test.txt
done

python3 one_kim_crf_media.py > logs/$today/one_kim_crf_media.log
for file in solutions/$today/one_kim_crf_media_train*; do
        echo $file >> solutions/$today/score_one_kim_crf_media_train.txt
	./colleval.sh $file >> solutions/$today/score_one_kim_crf_media_train.txt
        echo "\n" >> solutions/$today/score_one_kim_crf_media_train.txt
done
for file in solutions/$today/one_kim_crf_media_test*; do
        echo $file >> solutions/$today/score_one_kim_crf_media_test.txt
	./colleval.sh $file >> solutions/$today/score_one_kim_crf_media_test.txt
        echo "\n" >> solutions/$today/score_one_kim_crf_media_test.txt
done
