import numpy as np
import modules.importer as imp
from keras.preprocessing.sequence import pad_sequences

def words_to_idx(sentence):
    words_splitted = sentence.split()
    words_encodded = [imp.word2idx_dict.get(w, 3) for w in words_splitted]
    x_input = pad_sequences([words_encodded], maxlen = imp.max_length)
    return x_input 

def idx_to_labels(labels_encodded):
    to_int = np.array(labels_encodded, dtype=int)[0]
    y_output = [imp.idx2target_dict.get(l, '<?>') for l in to_int]
    return y_output
