#!/usr/bin/python3

import modules.evaluation as evl 
import modules.importer as imp

import numpy as np

# config tf for GPU
import tensorflow as tf
configT = tf.ConfigProto()
configT.gpu_options.allow_growth = True
session = tf.Session(config=configT)

# path for saved weigths

import os.path
path = "saved_models/kim_media.h5"

# solution
import datetime
now = datetime.datetime.now()
date_now = now.strftime("%Y-%m-%d")
relative_path = "solutions/" + date_now

import os
if not os.path.exists(relative_path):
    os.makedirs(relative_path)
test_path = "data/media.test.crf"
solution_path = "solutions/" + date_now + "/kim_media.txt" 

# load data 
importer = imp.importer("data/media.train.crf", "data/media.test.crf")
importer.run()

x_train, y_train = (importer.x_train_c, importer.y_train_c)
x_test, y_test = (importer.x_test_c, importer.y_test_c)

#(batch_size, input_size, emb_size) = x_train.shape

# print data info
importer.info()

from keras.models import Sequential, Model
from keras.layers import Input, Concatenate, TimeDistributed, Embedding 
from keras.layers import Dense, Bidirectional, CuDNNLSTM, Dropout 
from keras.layers import LSTM
from keras.layers import Conv1D, MaxPooling1D, GlobalMaxPooling1D, Flatten 

# parameters
char_dict_size = len(importer.char2idx_dict)
embedding_dim = 15
conv_layers = [(150, 1), 
               (150, 2), 
               (150, 3), 
               (150, 4), 
               (150, 5), 
               (150, 6),
	       (150, 7),
               (150, 8),
               (150, 9),
               (150, 10),
               (150, 11)]
filters_act = "tanh"
lstm_units = 500
lstm_layers = 2
drp = 0.5
label_size = len(importer.target2idx_dict)

# embedding model
def cnn_model():
    
    (batch_size, input_size, emb_size) = x_train.shape
    
    #filter_width = 2
    #feature_length -> avg of num of chars in word
    feature_length = max([int(np.average([len(w) for w in importer.words])),\
                        int(np.average([len(w) for w in importer.words_test]))])
    
    #print("length of the filters:\t {} on words of length {}\n".format(filter_width, emb_size))
    print("avg length of word:\t {} on words of max length {}\n".format(feature_length, emb_size))
    
    inp = Input((emb_size,))
    x = Embedding(len(importer.char2idx_dict), 15)(inp)

    conv_out = []
    for filter_num, filter_width in conv_layers:
        conv = Conv1D(filter_num, filter_width, activation='tanh')(x)
        pool = GlobalMaxPooling1D()(conv)
        conv_out.append(pool)

    concat_out = Concatenate()(conv_out)
    cnn_model = Model(inp, concat_out)
    cnn_model.summary()
    return cnn_model


#lstm model
def lstm_model():
    cnn_m = cnn_model() 

    (batch_size, input_size, emb_size) = x_train.shape
    lstm1 = Input(shape = (input_size, emb_size), dtype='int32')
    lstm2 = TimeDistributed(cnn_m)(lstm1)
    lstm3 = Bidirectional(CuDNNLSTM(100, return_sequences = True))(lstm2)
    lstm3_bis = Dropout(0.5)(lstm3)
    #lstm3_tris = Bidirectional(CuDNNLSTM(100, return_sequences=True))(lstm3_bis)
    #lstm4 = Dropout(0.5)(lstm3_tris)
    lstm5 = TimeDistributed(Dense(label_size, activation='softmax'))(lstm3_bis)
    lstm_model = Model(lstm1, lstm5)
    return lstm_model


def less_data(s_train_limit=1000, w_limit=0.25,c_limit=0.20):
    global x_train, y_train, x_test, y_test
    train_num_sent = x_train.shape[0]
    test_num_sent = x_test.shape[0]
    print("num of sentences:\t train {}; test {}".format(train_num_sent, test_num_sent))

    # limit sentences
    factor = train_num_sent / test_num_sent
    print("num train sentences on num test sentences:\t {}".format(factor))
    s_test_limit = int(s_train_limit / factor)
    print("limited dataset to: train - from {} to {}; test - from {} to {}"\
    .format(x_train.shape[0], s_train_limit, x_test.shape[0], s_test_limit))
    
    # limit words
    word_limit = int(x_train.shape[1] * w_limit)
    print("limited words in colon from {} to {}".format(x_train.shape[1], word_limit))
    
    # limit chars
    char_limit = int(x_train.shape[2] * c_limit)
    print("limited chars in colon from {} to {}".format(x_train.shape[2], char_limit))
    
    # limit all
    x_train = x_train[:s_train_limit, -word_limit:, -char_limit:]
    y_train = y_train[:s_train_limit, -word_limit:, -char_limit:]
    
    x_test = x_test[:s_test_limit, -word_limit:, -char_limit:]
    y_test = y_test[:s_test_limit, -word_limit:, -char_limit:]
    return

def print_prediction(sent, prediction):
    print(prediction.shape)
    for idx in range(prediction.shape[0]):
        min_label = np.argmin(prediction[idx])
        max_label = np.argmax(prediction[idx])
        
        #readable
        h_word = [importer.idx2char_dict[char] for char in sent[idx]]
        h_min_label = importer.idx2target_dict[min_label]
        h_max_label = importer.idx2target_dict[max_label]
        
        #print 
        print("word\n\t{}\n\t{}\nmin label:\n\t{}\n\t{}\nmax label\n\t{}\n\t{}\n".format(sent[idx], h_word,\
            min_label, h_min_label, max_label, h_max_label))

def try_prediction(model):
    my_inp = np.array(x_train[10:11])     
    prediction = model.predict(my_inp)
    
    print_prediction(my_inp[0], prediction[0])

def write_solution(model):
    
    predictions = model.predict(x_test)
    pred_seqs = predictions.argmax(-1)
    true_seqs = y_test.reshape(pred_seqs.shape)

    true = true_seqs[true_seqs>0]
    pred = pred_seqs[true_seqs>0] 

    true_t = np.array([importer.idx2target_dict[x] for x in true]) 
    pred_t = np.array([importer.idx2target_dict[x]for x in pred])

    # write solution
    test_file = open(test_path, 'r')
    solution_file = open(solution_path, 'w')

    idx_label = 0

    for line in test_file:
        if line == "\n":
            solution_file.write(line)
        else:
            line = line.rstrip('\n\r')
            line = line.split()
            solution_file.write(line[0] + ' ' + line[-1] + ' ' + pred_t[idx_label] + '\n')
            idx_label = idx_label + 1
    print("created a solution file: {}".format(solution_path))
    return 
    
#run it
def try_model(limit=False):
    # workaround for the third dimension
    #import numpy as np
   
    if limit:
        less_data()
        print("new dataset shapes. train: x {}, y {}".format(x_train.shape, y_train.shape))
        print("new dataset shapes. test: x {}, y {}".format(x_test.shape, y_test.shape))
   
    model = lstm_model()
    
    # load weights saved
    if os.path.exists(path):
    
        from keras.models import load_model
        print("Resume",)
        model.load_weights(path)	
        print(" -> Done")

    model.summary()
    model.compile('adam', 'sparse_categorical_crossentropy', metrics=['accuracy'])
    
    # model eval
    if(len(x_train.shape)>1):
        test_mask = np.sum(x_test, axis=2)
        train_mask = np.sum(x_train, axis=2)

        print("reference masking the zeros:\n\tshapes - y_test: {}\t x_test_mask {}".format\
        (y_test.shape, test_mask.shape))
    
        print("reference test evaluation masking the zeros:\n\tshapes - y_train: {}\t x_train_mask {}".format\
        (y_train.shape, train_mask.shape))
 
        ref = y_test[test_mask > 0]
        #refV = validY[x_train[0] > 0]
        refT = y_train[train_mask > 0]
    else:
        ref = y_test[x_test > 0]
        #refV = validY[x_train[0] > 0]
        refT = y_train[x_train > 0]

    evalFinal = evl.evalModel(ref,refT,refT, x_test, x_train, x_train, path, max=True)
    finalhyp = evalFinal.getBestHyp()
    #print ('Final result {:4.2f} '.format(f1_score(ref,finalhyp,average='micro')*100))


    model.fit(x_train, y_train, epochs=90, batch_size=128, callbacks=[evalFinal])#validation_split=0.1
    write_solution(model)
		
if __name__ == "__main__":
    
    try_model()
