'''
data format:
    WORD WORD \tTAG.TAG.TAG. ...\n
    NUMBER N_TAG N_TAG\n
    \n
where:
    - WORD is a German word and TAG is a word tag.
    - NUMBER is a number and N_TAG is a number tag.
'''
import numpy as np
import scipy.sparse as sps

import tensorflow as tf

from keras.preprocessing.sequence import pad_sequences 


class importer():
    def __init__(self, train_path, test_path):
        # words dictionaries
        self.word2idx_dict = dict()
        self.idx2word_dict = dict()
        
        self.target2idx_dict = dict()
        self.idx2target_dict = dict()
        
        self.max_length = 0
        self.max_length_c = 0
        self.max_length_words = 0

        # chars dictionary
        self.char2idx_dict = dict()
        self.idx2char_dict = dict()

        # encode dict
        self.encode_dict = dict()

        # word sentences
        self.sentences = []
        self.sentences_test = []
        
        self.words = []
        self.words_test = []

        self.labels = []
        self.labels_test = []
        
        # char sentences
        self.sentences_c = []
        self.sentences_c_test = []

        self.chars = []
        self.chars_test = []
        
        # 1 sentences of chars
        self.sentences_full_char = []
        self.sentences_full_char_test = []
        
        # path
        #self.train_path = "data/tiger.train" #"/data/NLU/ATIS/atis.train.crf"
        #self.test_path = "data/tiger.test" #"/data/NLU/ATIS/atis.test.crf"
        self.train_path = train_path
        self.test_path = test_path

        # x y
        self.x_train = []
        self.y_train = []

        self.x_test = []
        self.y_test = []

        self.x_train_c = []
        self.y_train_c = []

        self.x_test_c = []
        self.y_test_c = []
        
        self.x_train_full_char = []
        self.y_train_full_char = []

        self.x_test_full_char = []
        self.y_test_full_char = []


    def init_dict(self):
        # words dicts
        self.word2idx_dict["PADDING"] = 0
        self.word2idx_dict["<s>"] = 1
        self.word2idx_dict["</s>"] = 2
        self.word2idx_dict["UNK"] = 3

        self.idx2word_dict[0] = "PADDING"
        self.idx2word_dict[1] = "<s>"
        self.idx2word_dict[2] = "</s>"
        self.idx2word_dict[3] = "UNK"

        # chars dicts
        self.char2idx_dict["PADDING"] = 0
        self.char2idx_dict["<s>"] = 1
        self.char2idx_dict["</s>"] = 2
        self.char2idx_dict["<w>"] = 3
        self.char2idx_dict["</w>"] = 4
        self.char2idx_dict["UNK"] = 5

        self.idx2char_dict[0] = "PADDING"
        self.idx2char_dict[1] = "<s>"
        self.idx2char_dict[2] = "</s>"
        self.idx2char_dict[3] = "<w>"
        self.idx2char_dict[4] = "</w>"
        self.idx2char_dict[5] = "UNK"

        # labels dicts
        self.target2idx_dict["PAD"] = 0
        self.target2idx_dict["<S>"] = 1
        self.target2idx_dict["<?>"] = 2
        self.target2idx_dict["<W>"] = 3
        self.target2idx_dict["</W>"] = 4

        self.idx2target_dict[0] = "PAD"
        self.idx2target_dict[1] = "<S>"
        self.idx2target_dict[2] = "<?>"
        self.idx2target_dict[3] = "<W>"
        self.idx2target_dict[4] = "</W>"


    def read_data(self, path, bound_char, bound_sent = False): 
        words = []
        chars = []
        labels = []
        
        #with bound
        sentences = []
        sentences_c = []
        sentences_full_char = []
        
        s_buffer = []
        s_buffer_c = []
        s_buffer_full_char = []

        # add special starting symbol
        if(bound_sent):
            s_buffer.append(["<s>", "<S>"])
            s_buffer_c.append(["<s>", "<S>"])
            s_buffer_full_char.append(["<s>", "<S>"])
            
        data_file = open(path, 'r')

        for line in data_file:
            line = line.rstrip('\n\r')
            line = line.split()

            if line == []:

                # add special ending symbol
                if(bound_sent):
                    s_buffer.append(["</s>", "<S>"])
                    s_buffer_c.append(["</s>", "<S>"])
                    s_buffer_full_char.append(["</s>", "<S>"])

                sentences.append(s_buffer)
                sentences_c.append(s_buffer_c)
                sentences_full_char.append(s_buffer_full_char)

                s_buffer = []
                s_buffer_c = []
                s_buffer_full_char = []
                
                # add special starting symbol
                if(bound_sent):
                    s_buffer.append(["<s>", "<S>"])
                    s_buffer_c.append(["<s>", "<S>"])
                    s_buffer_full_char.append(["<s>", "<S>"])
            else:
                words.append(line[0])
                labels.append(line[-1])

                if(bound_char):
                   s_buffer_c.append(["<w>", "<W>"])
                   #s_buffer_full_char.append(["<w>", "<W>"])
                
                for char in list(line[0]):
                    chars.append(char)
                    s_buffer_full_char.append([char, line[-1]])
                    #s_buffer_c.append([char, line[-1]]) 
                    #s_buffer_c.append([char])

                s_buffer_c.append([[char for char in line[0]], line[-1]])
                if(bound_char):
                    s_buffer_c.append(["</w>", "</W>"])
                    s_buffer_full_char.append(["</w>", "</W>"])

                s_buffer.append([line[0], line[-1]])
        return words, chars, labels, sentences, sentences_c, sentences_full_char

    def one_hot_encoding(self, lemmas, word2idx_dict, idx2word_dict): 

        lemmas = np.unique(lemmas)
        indices = np.arange(len(word2idx_dict), lemmas.size + len(word2idx_dict))
        word2idx_dict.update(dict(zip(lemmas, indices)))
        idx2word_dict.update(dict(zip(indices, lemmas)))
    
    def init_encode_dict(self, max_length_words):
        
        blacklist = {"PADDING", "<s>", "</s>", "UNK", "<w>", "</w>"}
        encode = list()
        encode.append(np.repeat(0, max_length_words))
        encode.append(np.repeat(1, max_length_words))
        encode.append(np.repeat(2, max_length_words)) 
        encode.append(np.repeat(3, max_length_words))
        encode.append(np.repeat(4, max_length_words))
        encode.append(np.repeat(5, max_length_words))

        for w in self.idx2word_dict.values():
            if w not in blacklist:
                encode.append([self.char2idx_dict[c] for c in w]) 
        encode_padded = pad_sequences(encode, max_length_words)
        key_encode = self.idx2word_dict.values()
        self.encode_dict.update(dict(zip(key_encode, encode_padded)))

    def encode_sentences_full_char(self, sentences, max_length):
    
        x = pad_sequences([[self.char2idx_dict.get(w[0], 3)  for w in s] for s in sentences], \
            maxlen = max_length)
        y = pad_sequences([[self.target2idx_dict.get(w[1], 2) for w in s] for s in sentences],\
            maxlen = max_length)
        return x, y
    
    def encode_sentences_c(self, sentences,  max_length):

        x = pad_sequences([[self.encode_dict.get(w[0], np.repeat(5, self.max_length_words)) \
            for w in s] for s in sentences], maxlen = max_length)
        y = pad_sequences([[self.target2idx_dict.get(w[1], 2) for w in s] for s in sentences],\
            maxlen = max_length)
        return x, y

    def encode_sentences(self, sentences, max_length):

        x = pad_sequences([[self.word2idx_dict.get(w[0], 3)  for w in s] for s in sentences], \
            maxlen = max_length)
        y = pad_sequences([[self.target2idx_dict.get(w[1], 2) for w in s] for s in sentences],\
            maxlen = max_length)
        return x, y

    def run(self, bound_char=False):
       
        # init dictionaries
        self.init_dict()

        # read data
        self.words, self.chars,  self.labels, \
        self.sentences, self.sentences_c, \
        self.sentences_full_char = self.read_data(self.train_path, bound_char) 
        
        self.words_test, self.chars_test, self.labels_test, \
        self.sentences_test, self.sentences_c_test, \
        self.sentences_full_char_test = self.read_data(self.test_path, bound_char)
        
        # calculate max length for padding
        self.max_length = max([ max([len(s) for s in self.sentences]), \
            max([len(s) for s in self.sentences_test])])
        self.max_length_c = max([ max([len(s) for s in self.sentences_c]), \
            max([len(s) for s in self.sentences_c_test])])
        self.max_length_words = max([ max([len(w) for w in self.words]), \
            max([len(w) for w in self.words_test])])

        self.max_length_full_char = max([ max([len(s) for s in self.sentences_full_char]), \
            max([len(s) for s in self.sentences_full_char_test])])

        # encoding data 
        self.one_hot_encoding(self.words, self.word2idx_dict, self.idx2word_dict)
        self.one_hot_encoding(self.chars, self.char2idx_dict, self.idx2char_dict) 
        self.one_hot_encoding(self.labels, self.target2idx_dict, self.idx2target_dict)

        # init encode_dict
        self.init_encode_dict(self.max_length_words)

        # get train set
        self.x_train, self.y_train = self.encode_sentences(self.sentences, \
            self.max_length)
        
        # get train c set
        self.x_train_c, self.y_train_c = self.encode_sentences_c(self.sentences, \
            self.max_length)

        # get test set
        self.x_test, self.y_test = self.encode_sentences(self.sentences_test, \
            self.max_length) 
        
        # get test set
        self.x_test_c, self.y_test_c = self.encode_sentences_c(self.sentences_test, \
            self.max_length)

        # get train full char
        self.x_train_full_char, self.y_train_full_char = self.encode_sentences_full_char(self.sentences_full_char, \
            self.max_length_full_char)
            
        self.x_test_full_char, self.y_test_full_char = self.encode_sentences_full_char(self.sentences_full_char_test, \
            self.max_length_full_char)   

        # expand dimension
        self.y_train = np.expand_dims(self.y_train, 2)
        self.y_test = np.expand_dims(self.y_test, 2)

        self.y_train_c = np.expand_dims(self.y_train_c, 2)
        self.y_test_c = np.expand_dims(self.y_test_c, 2)
        
        self.y_train_full_char = np.expand_dims(self.y_train_full_char, 2)
        self.y_test_full_char = np.expand_dims(self.y_test_full_char, 2)
    
    def info(self):

        w_size = len (self.word2idx_dict)
        l_size = len (self.target2idx_dict)
        c_size = len (self.char2idx_dict)

        print("-----------------------------------------------")
        print("DATA INFO")
        print("-----------------------------------------------")
        print ("Dictionaries Size:")
        print ("Words:\t{} Labels:\t{} Chars:\t{}".format(w_size, l_size, c_size))
        print("\n")

        w_x_shape = self.x_train.shape
        w_y_shape = self.y_train.shape

        w_x_t_shape = self.x_test.shape
        w_y_t_shape = self.y_test.shape
        
        c_x_shape = self.x_train_c.shape
        c_y_shape = self.y_train_c.shape

        c_x_t_shape = self.x_test_c.shape
        c_y_t_shape = self.y_test_c.shape

        fc_x_shape = self.x_train_full_char.shape
        fc_y_shape = self.y_train_full_char.shape 
        
        fc_x_t_shape = self.x_test_full_char.shape
        fc_y_t_shape = self.y_test_full_char.shape

        print("Words data Shapes")
        print ("Train X:\t{} Train Y:\t{}".format(w_x_shape, w_y_shape))
        print ("Test  X:\t{} Test  Y:\t{}".format(w_x_t_shape, w_y_t_shape))
        print("\n")
        print("Chars data Shapes")
        print ("Train X:\t{} Train Y:\t{}".format(c_x_shape, c_y_shape))
        print ("Test  X:\t{} Test  Y:\t{}".format(c_x_t_shape, c_y_t_shape))
        print("\n")
        print("Full Chars data Shapes")
        print ("Train X:\t{} Train Y:\t{}".format(fc_x_shape, fc_y_shape))
        print ("Test  X:\t{} Test  Y:\t{}".format(fc_x_t_shape, fc_y_t_shape))
        print("\n")
 
        w_l = max([len(s) for s in self.sentences])
        w_l_test = max([len(s) for s in self.sentences_test])
        c_l = max([len(s) for s in self.sentences_c])
        c_l_test = max([len(s) for s in self.sentences_c_test])
        c_l_b = max([len(list(w))for w in self.words])
        c_l_b_test = max([len(list(w))for w in self.words_test])

        print("Max length of a Sentence")
        print ("Words Train:\t{} Test:\t{}".format(w_l, w_l_test))
        print("Chars Train:\t{} Test:\t{}".format(c_l, c_l_test))
        print("\n")
        
        print("Max length of a Word")
        print ("Train:\t{} Test:\t{}".format(c_l_b, c_l_b_test))
        print("\n")

        s_l = len(self.sentences)
        s_l_test = len(self.sentences_test)
        c_s_l = len(self.sentences_c)
        c_s_l_test = len(self.sentences_c_test)

        print("Number of Sentences")
        print ("Words Train:\t{} Test:\t{}".format(s_l, s_l_test)) 
        print ("Chars Train:\t{} Test:\t{}".format(c_s_l, c_s_l_test))
        print("-----------------------------------------------")
