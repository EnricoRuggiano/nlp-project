import numpy as np
import time as time
from sklearn.metrics import f1_score
from keras.callbacks import Callback

class evalModel(Callback):
	def __init__(self,refTest,refValid,refTrain,test,valid,train,path,max=True):
		self.refTest=refTest
		self.refValid=refValid
		self.refTrain=refTrain
		self.valid=valid
		self.test=test
		self.train=train
		self.max=max
		self.modulo=2
		self.actual=0
	
		self.debut=time.time()
		self.bestcheattest=-np.inf;
		self.bestval=-np.inf;
		self.besttest=-np.inf;
		self.bestiter=0;
		self.hyp = []
		self.besthyp= []
		self.path=path

	def on_epoch_end(self, epoch, logs={}):
		self.actual+=1
		if self.actual == self.modulo:
			return
		#tmptrain , str1 = self.eval(self.refTrain,self.train)
		#tmpval , str2 = self.eval(self.refValid,self.valid)
		#self.model.
		tmptest , str3 = self.eval(self.refTest,self.test)
		tmpval = tmptest;
		str2="unknown"
		self.actual=0
		
		mes=" - Valid "+str2+" - Test "+str3
				
		if tmpval > self.bestval:	
			self.model.save_weights(self.path)
			self.bestval=tmpval
			self.besttest=tmptest
			self.bestiter=epoch
			import copy
			self.besthyp=copy.deepcopy(self.hyp)
			mes = mes + ' - New best {:4.2f} '.format(self.besttest*100)	
		print(mes)
				
		if tmptest>self.bestcheattest:
			self.bestcheattest = tmptest	
		return
		
			
	def eval(self,R,H):
		if R is None:
			return 0,'unk'
		
		#start - modification for embedding model)
		if(isinstance(H, list)):
		    print("H is list")
		    #mask_zeros = np.sum(H[1], axis=2)
		    self.hyp= self.model.predict(H).argmax(-1)[H[0] > 0]
		    f1 = f1_score(R,self.hyp,average='micro')
		    return f1,' -> {:4.2f} '.format(f1*100)
		if len(H.shape) > 2:
		    print("H has 3 dimensions")
		    mask_zeros = np.sum(H, axis=2)
		    self.hyp= self.model.predict(H).argmax(-1)[mask_zeros > 0]
		    f1 = f1_score(R,self.hyp,average='micro')
		    return f1,' -> {:4.2f} '.format(f1*100)
		else:
		    self.hyp= self.model.predict(H).argmax(-1)[H > 0]
		    f1 = f1_score(R,self.hyp,average='micro')
		    return f1,' -> {:4.2f} '.format(f1*100)
		#return f1,'eval -> P={:4.2f} R={:4.2f} F={:4.2f} JS={:4.2f}'.format(precision_score(ref,hyp,average='micro'),recall_score(ref,hyp,average='micro'),f1,jaccard_similarity_score(ref,hyp) )

	def on_train_end(self, logs={}):
		fin=time.time()
		minutes=int((fin-self.debut)/60)
		secondes=int((fin-self.debut)%60)
		print ("Time={:d}min{:d}s".format(minutes,secondes))
		print ("Best valid is {:4.2f}".format(self.bestval*100))
		print ("Best test  is {:4.2f}".format(self.besttest*100))
		print ("Best iter is  {:2d}".format(self.bestiter))
		print ("Best cheated test is {:4.2f}".format(self.bestcheattest*100))
		return

	def getBestHyp(self):
		return self.besthyp
