import numpy as np
import time as time
from sklearn.metrics import f1_score
from keras.callbacks import Callback

class evalModel(Callback):
	def __init__(self,refTest,refValid,refTrain,x_test, y_test, y_train, valid, train, path, solution_path, test_path, train_path, target_dict, max=True):
		self.refTest=refTest
		self.refValid=refValid
		self.refTrain=refTrain
		self.valid=valid
		self.x_test=x_test
		self.y_test=y_test
		self.train=train
		self.y_train=y_train
		self.max=max
		self.modulo=2
		self.actual=0
	
		self.debut=time.time()
		self.bestcheattest=-np.inf;
		self.bestval=-np.inf;
		self.besttest=-np.inf;
		self.bestiter=0;
		self.hyp = []
		self.besthyp= []
		self.path=path

		self.solution_path=solution_path
		self.test_path=test_path
		self.train_path = train_path
		self.target_dict=target_dict

	def on_epoch_end(self, epoch, logs={}):
		self.actual+=1
		if self.actual == self.modulo:
			return
		#tmptrain , str1 = self.eval(self.refTrain,self.train)
		#tmpval , str2 = self.eval(self.refValid,self.valid)
		#self.model.
		tmptest , str3 = self.eval(self.refTest,self.x_test)
		tmpval = tmptest;
		str2="unknown"
		self.actual=0
		
		mes=" - Valid "+str2+" - Test "+str3
				
		if tmpval > self.bestval:	
			self.model.save_weights(self.path)
			self.bestval=tmpval
			self.besttest=tmptest
			self.bestiter=epoch
			import copy
			self.besthyp=copy.deepcopy(self.hyp)
			mes = mes + ' - New best {:4.2f} '.format(self.besttest*100)	
		print(mes)
				
		if tmptest>self.bestcheattest:
			self.bestcheattest = tmptest
		
		# write file
		self.write_solution(epoch, "train", self.train_path, self.train, self.y_train)			
		self.write_solution(epoch, "test", self.test_path, self.x_test, self.y_test)	
		return
		
			
	def eval(self,R,H):
		if R is None:
			return 0,'unk'
		
		#start - modification for embedding model)
		if(isinstance(H, list)):
		    print("H is list")
		    #mask_zeros = np.sum(H[1], axis=2)
		    self.hyp= self.model.predict(H).argmax(-1)[H[0] > 0]
		    f1 = f1_score(R,self.hyp,average='micro')
		    return f1,' -> {:4.2f} '.format(f1*100)
		if len(H.shape) > 2:
		    print("H has 3 dimensions")
		    mask_zeros = np.sum(H, axis=2)
		    self.hyp= self.model.predict(H).argmax(-1)[mask_zeros > 0]
		    f1 = f1_score(R,self.hyp,average='micro')
		    return f1,' -> {:4.2f} '.format(f1*100)
		else:
		    self.hyp= self.model.predict(H).argmax(-1)[H > 0]
		    f1 = f1_score(R,self.hyp,average='micro')
		    return f1,' -> {:4.2f} '.format(f1*100)
		#return f1,'eval -> P={:4.2f} R={:4.2f} F={:4.2f} JS={:4.2f}'.format(precision_score(ref,hyp,average='micro'),recall_score(ref,hyp,average='micro'),f1,jaccard_similarity_score(ref,hyp) )

	def on_train_end(self, logs={}):
		fin=time.time()
		minutes=int((fin-self.debut)/60)
		secondes=int((fin-self.debut)%60)
		print ("Time={:d}min{:d}s".format(minutes,secondes))
		print ("Best valid is {:4.2f}".format(self.bestval*100))
		print ("Best test  is {:4.2f}".format(self.besttest*100))
		print ("Best iter is  {:2d}".format(self.bestiter))
		print ("Best cheated test is {:4.2f}".format(self.bestcheattest*100))
		return

	def getBestHyp(self):
		return self.besthyp

	def write_solution(self, epoch, suffix, data_path, x_data, y_data):
		predictions = self.model.predict(x_data)
		pred_seqs = predictions.argmax(-1)
		true_seqs = y_data.reshape(pred_seqs.shape)

		true = true_seqs[true_seqs>0]
		pred = pred_seqs[true_seqs>0] 

		true_t = np.array([self.target_dict[x] for x in true]) 
		pred_t = np.array([self.target_dict[x]for x in pred])

		# write solution
		epoch_solution_file_path = self.solution_path + "_" + suffix + str(epoch + 1) + ".txt"
		test_file = open(data_path, 'r')
		solution_file = open(epoch_solution_file_path, 'w')

		idx_label = 0

		for line in test_file:
			if line == "\n":
				solution_file.write(line)
			else:
				line = line.rstrip('\n\r')
				line = line.split()
				solution_file.write(line[0] + ' ' + line[-1] + ' ' + pred_t[idx_label] + '\n')
				idx_label = idx_label + 1
		print("created a solution file: {}".format(epoch_solution_file_path))
		return 
