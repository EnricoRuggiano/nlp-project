#!/usr/bin/python3

import modules.evaluation as evl 
import modules.importer as imp

# config tf for GPU
import tensorflow as tf
configT = tf.ConfigProto()
configT.gpu_options.allow_growth = True
session = tf.Session(config=configT)

# path for saved weigths

import os.path
path = "model/emb_model.h5"
condition = os.path.exists(path)

# load data 
importer = imp.importer()
importer.run()

x_train, y_train = (importer.x_train_c, importer.y_train_c)
x_test, y_test = (importer.x_test_c, importer.y_test_c)

(batch_size, input_size) = x_train.shape

# print data info
importer.info()

from keras.models import Sequential, Model
from keras.layers import Input, concatenate, TimeDistributed, Embedding 
from keras.layers import Dense, Bidirectional, CuDNNLSTM, Dropout 
from keras.layers import LSTM
from keras.layers import Conv1D, GlobalMaxPooling1D 

# parameters
dict_size = len(importer.char2idx_dict)
embedding_dim = 15
filters_width = [1, 2, 3, 4, 5, 6, 7]
filters_dim = [min(200, 50 * width) for width in filters_width]
filters_act = "tanh"
stm_units = 500
stm_layers = 2
drp = 0.5
label_size = len(importer.target2idx_dict)

# layers
inp_layer = Input(shape = (input_size,), dtype = 'int32')
emb_layer = Embedding(dict_size, embedding_dim, mask_zero=False)(inp_layer)
conv_layers = []

# convolutional layers
for i in range(len(filters_dim)):
    c_layer = Conv1D(filters_dim[i], filters_width[i], activation = filters_act, strides = 1)(emb_layer)
    c_polling = GlobalMaxPooling1D()(c_layer);
    conv_layers.append(c_polling)

# concatenate layer
feature_layer = concatenate(conv_layers)

# time distributed
#feature_time_distr_layer = TimeDistributed(feature_layer)
emb_feature_layer = Embedding(1100, 1, mask_zero=False)(feature_layer)

# LSTM layer
#stm_layer = Bidirectional(CuDNNLSTM(stm_units, return_sequences = True))(feature_layer)
lstm_layer = Bidirectional(LSTM(stm_units, return_sequences = True))(emb_feature_layer)
drp_layer = Dropout(drp)(lstm_layer)

# output layer
out_layer = TimeDistributed(Dense(label_size, activation='softmax'))(drp_layer)

model = Model(inputs=inp_layer, outputs=out_layer)
model.compile('adam', 'sparse_categorical_crossentropy', metrics=['accuracy'])
model.summary()

if condition:
    from keras.models import load_model
    print("Resume",)
    model.load_weights(path)	
    print(" -> Done")

# model eval

ref = y_test[x_test > 0]
#refV = validY[x_train[0] > 0]
refT = y_train[x_train > 0]

evalFinal = evl.evalModel(ref,refT,refT, x_test,x_train,x_train,path, max=True)
finalhyp = evalFinal.getBestHyp()
#print ('Final result {:4.2f} '.format(f1_score(ref,finalhyp,average='micro')*100))

model.fit(x_train, y_train, epochs=2, batch_size=128, callbacks=[evalFinal])#validation_split=0.1

#model.evaluate(x_test, y_test)

